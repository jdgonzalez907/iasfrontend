import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SearchForm } from 'src/app/models/search.form'

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  model = new SearchForm(null, null);
  zoneList = [];
  dataSource = [];
  displayedColumns: string[] = ['cdave', 'dsnombrecomun', 'dsnombrecientifico', 'actions'];

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getZones();
    this.getDataSource();
  }

  onSubmit() {
    this.getDataSource();
  }

  getZones() {
    this.http.get(
      environment.baseUrl + 'zonas/'
    ).subscribe((data: any[]) => {
      this.zoneList = data;
    });
  }

  getDataSource() {
    let url;
    if (!this.model.name && !this.model.zone) {
      url = 'aves';
    } else {
      url = 'aves/search?name=' + this.model.name + '&zone=' + this.model.zone;
    }
    this.http.get(
      environment.baseUrl + url
    ).subscribe((data: any) => {
      this.dataSource = data.content;
    });
  }

  openDialog (cdave) {
    let isDelete = confirm('Desea eliminar este item?');
    if (isDelete) {
      this.http.delete(
        environment.baseUrl + 'aves/' + cdave
      ).subscribe(response => {
        this.getDataSource();
      })
    }
  }

}
