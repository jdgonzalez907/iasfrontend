import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { AveForm } from 'src/app/models/ave.form'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  model = new AveForm(null, null, null);
  errors = {
    form: [],
    cdave: [],
    dsnombrecomun: [],
    dsnombrecientifico: []
  };

  id: string = null;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if ('id' in params) {
        this.id = params['id']
        this.http.get(
          environment.baseUrl + 'aves/' + this.id
        ).subscribe((data: AveForm) => {
          this.model = data;
        });
      }
    })
  }

  onSubmit() {
    this.errors = {
      form: [],
      cdave: [],
      dsnombrecomun: [],
      dsnombrecientifico: []
    };
    let request = null;
    if (this.id !== null) {
      request = this.http.put(
        environment.baseUrl + 'aves/' + this.id,
        this.model
      )
    } else {
      request = this.http.post(
        environment.baseUrl + 'aves',
        this.model
      )
    }
    request.subscribe(
      (data: any) => {
        this.router.navigate(['/']);
      },
      (errorResponse: any) => {
        if (errorResponse.error.errors) {
          errorResponse.error.errors.forEach(element => {
            this.errors[element.field].push(element.defaultMessage)
          });
        } else {
          this.errors.form = [errorResponse.error.message]
        }
      }
    )
  }

}
