import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { AveForm } from 'src/app/models/ave.form'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  model = new AveForm(null, null, null);

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.http.get(
        environment.baseUrl + 'aves/' + params['id']
      ).subscribe((data: AveForm) => {
        this.model = data;
      });
    })
  }

}
